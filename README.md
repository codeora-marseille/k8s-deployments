1. apply the secrets 

```
$  kubectl apply -f security-secrets.yaml
```

2. apply PV and PVC

```
$ kubectl apply -f security-mysql-pv.yaml
```

3. apply database service/deployment

```
kubectl apply -f security-db-deployment.yaml
```

4. describe deployment

```
$ kubectl describe deployment security-mysql

$ kubectl get pods -l app=security-mysql
NAME                              READY   STATUS                       RESTARTS   AGE
security-mysql-696b574897-cdb29   0/1     CreateContainerConfigError   0          2m20s

$ kubectl describe pvc security-mysql-pv
```

5. On macOS visit `localhost:30010` if you are using Docker Desktop

If you are using minikube on Linux, run 

```
$ minikube ip
192.168.99.103
```

And visit `192.168.99.103:30010`